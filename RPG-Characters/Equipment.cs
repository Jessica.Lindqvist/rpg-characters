﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    class Equipment
    {
        public static void EquipItems(string chosenHero, int myHeroLevel) //Have the choosen hero and the level from program page
        {
            int baseDam = 10; //Base damage of low level weapons
            int baseDamHighLvl = 15; //Base damage of high level weapons
            int WeapDam = baseDam * myHeroLevel; //The weapon damage is base damage * heros level
            int WeapDamHighLvl = baseDamHighLvl * myHeroLevel; //The weapon damage is base damage * heros level

            string weaponChoise; //How to choose a weapon
            Console.WriteLine(" ");
            Console.WriteLine("choose an weapon to equip:\n Axe\n Bow\n Dagger\n Hammer\n Staff\n Sword\n Wand");
            weaponChoise = Console.ReadLine().ToLower();
            try
            {
                switch (weaponChoise) //Switch case for weapon choice
                {
                    case "wand":
                        Weapons.WeaponType wand = Weapons.WeaponType.Wand; //From the weapons page, the weapon
                        ItemSlot itemSlotWeapon = ItemSlot.WEAPON_SLOT; //Not sure what to do with, might delete later
                        if (chosenHero == "mage" && myHeroLevel >= 1) //Check if you can equip the weapon based on if you have the correct hero and level
                        {
                            Console.WriteLine("You equiped an item! " + wand + " in " + itemSlotWeapon); //Show the succesfull equipment of the weapon
                            Console.WriteLine("Your weapons damage is: " + WeapDam); //Show what damage the weapon does
                        }
                        else
                        {
                            throw new CustomExeptions(); //If you try to equip the weapon while being the wrong hero or level throw a custom exeption
                        }
                        break;
                    case "axe":
                        Weapons.WeaponType axe = Weapons.WeaponType.Axe; //From the weapons page, the weapon
                        ItemSlot itemSlotWeaponAxe = ItemSlot.WEAPON_SLOT; //Not sure what to do with, might delete later
                        if (chosenHero == "warrior" && myHeroLevel >= 1) //Check if you can equip the weapon based on if you have the correct hero and level
                        {
                            Console.WriteLine("You equiped an item! " + axe + " in " + itemSlotWeaponAxe); //Show the succesfull equipment of the weapon
                            Console.WriteLine("Your weapons damage is: " + WeapDam); //Show what damage the weapon does
                        }
                        else
                        {
                            throw new CustomExeptions(); //If you try to equip the weapon while being the wrong hero or level throw a custom exeption
                        }
                        break;
                    case "bow":
                        Weapons.WeaponType bow = Weapons.WeaponType.Bow;
                        ItemSlot itemSlotWeaponBow = ItemSlot.WEAPON_SLOT;
                        if (chosenHero == "ranger" && myHeroLevel >= 1)
                        {
                            Console.WriteLine("You equiped an item! " + bow + " in " + itemSlotWeaponBow);
                            Console.WriteLine("Your weapons damage is: " + WeapDam);
                        }
                        else
                        {
                            throw new CustomExeptions();
                        }
                        break;
                    case "dagger":
                        Weapons.WeaponType dagger = Weapons.WeaponType.Dagger;
                        ItemSlot itemSlotWeaponDagger = ItemSlot.WEAPON_SLOT;
                        if (chosenHero == "rogue" && myHeroLevel >= 1)
                        {
                            Console.WriteLine("You equiped an item! " + dagger + " in " + itemSlotWeaponDagger);
                            Console.WriteLine("Your weapons damage is: " + WeapDam);
                        }
                        else
                        {
                            throw new CustomExeptions();
                        }
                        break;
                    case "hammer":
                        Weapons.WeaponType hammer = Weapons.WeaponType.Hammer;
                        ItemSlot itemSlotWeaponHammer = ItemSlot.WEAPON_SLOT;
                        if (chosenHero == "warrior" && myHeroLevel >= 3)
                        {
                            Console.WriteLine("You equiped an item! " + hammer + " in " + itemSlotWeaponHammer);
                            Console.WriteLine("Your weapons damage is: " + WeapDamHighLvl);
                        }
                        else
                        {
                            throw new CustomExeptions();
                        }
                        break;
                    case "staff":
                        Weapons.WeaponType staff = Weapons.WeaponType.Staff;
                        ItemSlot itemSlotWeaponStaff = ItemSlot.WEAPON_SLOT;
                        if (chosenHero == "mage" && myHeroLevel >= 5)
                        {
                            Console.WriteLine("You equiped an item! " + staff + " in " + itemSlotWeaponStaff);
                            Console.WriteLine("Your weapons damage is: " + WeapDamHighLvl);
                        }
                        else
                        {
                            throw new CustomExeptions();
                        }
                        break;
                    case "sword":
                        Weapons.WeaponType sword = Weapons.WeaponType.Sword;
                        ItemSlot itemSlotWeaponSword = ItemSlot.WEAPON_SLOT;
                        if (chosenHero == "warrior" && myHeroLevel >= 5 || chosenHero == "rogue" && myHeroLevel >= 7)
                        {
                            Console.WriteLine("You equiped an item! " + sword + " in " + itemSlotWeaponSword);
                            Console.WriteLine("Your weapons damage is: " + WeapDamHighLvl);
                        }
                        else
                        {
                            throw new CustomExeptions();
                        }
                        break;
                    default:
                        Console.WriteLine("Invalid choise");
                        break;
                }
            }
            catch (CustomExeptions ex)
            {
                Console.WriteLine(ex.Message); //The custom exeption error message shows here
            }

        }
        public static void ArmorEquip(string chosenHero, int myHeroLevelArmor) //Have the choosen hero and the level from program page
        {
            int armorDam = 10; //Base damage from low level armor
            int armorDamHighLvl = 15; //Base damage from high level armor
            int armorPlate = 20; //Base damage from plate level armor

            string armorChoise;//How to choose armor
            Console.WriteLine("choose armor to equip:\n Cloth\n Leaxther\n Mail\n Plate");
            armorChoise = Console.ReadLine().ToLower();
            
            try
            {
                switch (armorChoise) //Switch case for armor choice
                {
                    case "cloth":
                        Armor.ArmorType cloth = Armor.ArmorType.Cloth; //From the armor page, the armor
                        if (chosenHero == "mage" && myHeroLevelArmor >= 1) //Check if you can equip the armor based on if you have the correct hero and level
                        {
                            Console.WriteLine("You equiped an item! " + cloth + " armor"); //Show the succesfull equipment of the armor
                             Console.WriteLine("Your new damage is: " + armorDam); //Show the armor damage
                                
                            }
                        else
                        {
                            throw new CustomExeptions(); //If you try to equip the armor while being the wrong hero or level throw a custom exeption
                        }
                        break;
                    case "leather":
                        Armor.ArmorType leather = Armor.ArmorType.Leather; //From the armor page, the armor
                        if (chosenHero == "ranger" && myHeroLevelArmor >= 1 || chosenHero == "rogue" && myHeroLevelArmor >= 1) //Check if you can equip the armor based on if you have the correct hero and level
                        {
                            Console.WriteLine("You equiped an item! " + leather + " armor"); //Show the succesfull equipment of the armor
                            Console.WriteLine("Your new damage is: " + armorDam); //Show the armor damage
                        }
                        else
                        {
                            throw new CustomExeptions(); //If you try to equip the armor while being the wrong hero or level throw a custom exeption
                        }
                        break;
                    case "mail":
                        Armor.ArmorType mail = Armor.ArmorType.Mail;
                        if (chosenHero == "ranger" && myHeroLevelArmor >= 5 || chosenHero == "rogue" && myHeroLevelArmor >= 7 || chosenHero == "warrior" && myHeroLevelArmor >= 1)
                        {
                            Console.WriteLine("You equiped an item! " + mail + " armor");
                            Console.WriteLine("Your new damage is: " + armorDamHighLvl);
                        }
                        else
                        {
                            throw new CustomExeptions();
                        }
                        break;
                    case "plate":
                        Armor.ArmorType plate = Armor.ArmorType.Plate;
                        if (chosenHero == "warrior" && myHeroLevelArmor >= 10)
                        {
                            Console.WriteLine("You equiped an item! " + plate + " armor");
                            Console.WriteLine("Your new armor damage is: " + armorPlate);
                        }
                        else
                        {
                            throw new CustomExeptions();
                        }
                        break;
                    default:
                        Console.WriteLine("Invalid choise");
                            break;
                }
                
            }
                
            catch (CustomExeptions ex)
            {
                    Console.WriteLine(ex.Message); //The custom exeption error message shows here
            }
            
        }
    }
}
