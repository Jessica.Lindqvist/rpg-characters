﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public class CustomExeptions : Exception
    {
        public CustomExeptions()
        {
        }

        public CustomExeptions(string message) : base(message)
        {

        }

        public override string Message => "Your Hero cant equip this item!";
    }
}
