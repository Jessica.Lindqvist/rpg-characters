﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public enum ItemSlot
    {
        WEAPON_SLOT,
        HEAD_ARMOR_SLOT,
        CHEST_ARMOR_SLOT,
        LEG_ARMOR_SLOT
    }
}
