﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    class Leveling
    {
        public static int AddLevel(Hero heroLevel)
        {
            heroLevel.level += 1;
            return heroLevel.level;

        }
        public static void AddMageAttribute(Mage mageInt)
        {
            mageInt.intelligence += 5;
            mageInt.strength += 1;
            mageInt.dexterity += 1;
            mageInt.damage += 5;

            Console.WriteLine("New Str: " + mageInt.strength);
            Console.WriteLine("New Dex: " + mageInt.dexterity);
            Console.WriteLine("New Int: " + mageInt.intelligence);
            Console.WriteLine("New Damage: " + mageInt.damage);
        }
        public static void AddRangerAttribute(Ranger rangeInt)
        {
            rangeInt.intelligence += 1;
            rangeInt.strength += 1;
            rangeInt.dexterity += 5;
            rangeInt.damage += 5;

            Console.WriteLine("New Str: " + rangeInt.strength);
            Console.WriteLine("New Dex: " + rangeInt.dexterity);
            Console.WriteLine("New Int: " + rangeInt.intelligence);
            Console.WriteLine("New Damage: " + rangeInt.damage);
        }
        public static void AddRogueAttribute(Rogue rogueInt)
        {
            rogueInt.intelligence += 1;
            rogueInt.strength += 1;
            rogueInt.dexterity += 4;
            rogueInt.damage += 4;

            Console.WriteLine("New Str: " + rogueInt.strength);
            Console.WriteLine("New Dex: " + rogueInt.dexterity);
            Console.WriteLine("New Int: " + rogueInt.intelligence);
            Console.WriteLine("New Damage: " + rogueInt.damage);
        }
        public static void AddWarriorAttribute(Warrior warInt)
        {
            warInt.intelligence += 1;
            warInt.strength += 2;
            warInt.dexterity += 3;
            warInt.damage += 2;

            Console.WriteLine("New Str: " + warInt.strength);
            Console.WriteLine("New Dex: " + warInt.dexterity);
            Console.WriteLine("New Int: " + warInt.intelligence);
            Console.WriteLine("New Damage: " + warInt.damage);
        }
    }
}
