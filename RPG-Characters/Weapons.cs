﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public class Weapons : Items
    {
        public Weapons(string name, int level, ItemSlot wEAPON_SLOT, WeaponType axe)
        {
            Name = name;
            Level = level;
            WEAPON_SLOT = wEAPON_SLOT;
            Axe = axe;
        }

        public string Name { get; }
        public int Level { get; }
        public ItemSlot WEAPON_SLOT { get; }
        public WeaponType Axe { get; }

        public enum WeaponType
        {
            Axe,
            Bow,
            Dagger,
            Hammer,
            Staff,
            Sword,
            Wand
        }
    }
}
