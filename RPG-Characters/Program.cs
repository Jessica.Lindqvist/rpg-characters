﻿using System;
using System.Collections.Generic;

namespace RPG_Characters
{
    class Program
    {      
        static void Main(string[] args)
        {
            string hero;
                //Add the data from the Heros class
                Mage myHeroMage = new Mage();
                Ranger myHeroRanger = new Ranger();
                Rogue myHeroRogue = new Rogue();
                Warrior myHeroWarrior = new Warrior();
                Hero myHero = new Hero();
                
            //Variables used to navigate and check the user input/data
                int mageAt = 0;
                int rangerAt = 0;
                int rogueAt = 0;
            int myHeroLevel = 1;
            string EquipmentCheck = "";

                string myHeroName = myHero.heroName;
                Console.Write("Enter your name: "); //You can enter your name!
                myHeroName = Console.ReadLine();
                Console.WriteLine("Hello " + myHeroName);
              
            //User chooses what Hero they want to be, and if they enter invalid input it will display the choices again
                bool correctHero = true;
                do {  
                Console.WriteLine("Choose a class:\n" + "Mage\n" + "Ranger\n" + "Rogue\n" + "Warrior\n");
                hero = Console.ReadLine().ToLower();

            switch (hero) //Switch case so the correct hero data is displaid
            {
                case "mage": //If user input is mage
                    Console.WriteLine(" ");
                    Console.WriteLine("You choose the Mage " + myHeroName); //Display all data the hero has (their name, intelligence, dexterity, level...)
                    Console.WriteLine("Your attributes:" );
                    Console.WriteLine("strength: " + myHeroMage.strength);
                    Console.WriteLine("dexterity: " + myHeroMage.dexterity);
                    Console.WriteLine("intelligence: " + myHeroMage.intelligence);
                    Console.WriteLine("Your level: " + myHeroMage.level );
                    Console.WriteLine("Your damage: " + myHero.damage);
                        EquipmentCheck = "mage"; //Set so right hero can equip correct weapons and armor
                        correctHero = false; //Break the loop
                            mageAt = 1; //Add so right hero will level up
                    break;
                case "ranger": //If user input is ranger
                    Console.WriteLine(" ");
                    Console.WriteLine("You choose the Ranger " + myHeroName);
                    Console.WriteLine("Your attributes:");
                    Console.WriteLine("strength: " + myHeroRanger.strength);
                    Console.WriteLine("dexterity: " + myHeroRanger.dexterity);
                    Console.WriteLine("intelligence: " + myHeroRanger.intelligence);
                    Console.WriteLine("Your level: " + myHeroRanger.level);
                    Console.WriteLine("Your damage: " + myHero.damage);
                        EquipmentCheck = "ranger";
                        correctHero = false;
                            rangerAt = 1;
                            break;
                case "rogue":
                    Console.WriteLine(" ");
                    Console.WriteLine("You choose the Rogue " + myHeroName);
                    Console.WriteLine("Your attributes:");
                    Console.WriteLine("strength: " + myHeroRogue.strength);
                    Console.WriteLine("dexterity: " + myHeroRogue.dexterity);
                    Console.WriteLine("intelligence: " + myHeroRogue.intelligence);
                    Console.WriteLine("Your level: " + myHeroRogue.level);
                    Console.WriteLine("Your damage: " + myHero.damage);
                        EquipmentCheck = "rogue";
                        correctHero = false;
                            rogueAt = 1;
                            break;
                case "warrior":
                    Console.WriteLine(" ");
                    Console.WriteLine("You choose the Warrior " + myHeroName);
                    Console.WriteLine("Your attributes:");
                    Console.WriteLine("strength: " + myHeroWarrior.strength);
                    Console.WriteLine("dexterity: " + myHeroWarrior.dexterity);
                    Console.WriteLine("intelligence: " + myHeroWarrior.intelligence);
                    Console.WriteLine("Your level: " + myHeroWarrior.level);
                    Console.WriteLine("Your damage: " + myHero.damage);
                        EquipmentCheck = "warrior";
                        correctHero = false;
                            break; //Dont need to check for level up only the before ones
                default:
                    Console.WriteLine("Not a valid choice"); //If user inputs something other then a hero
                    break;
            }
                }while (correctHero == true);

                bool addLevelUp = true;
                string levleUp;
                Console.WriteLine(" ");
                Console.WriteLine("Do you wish to level up?\n yes / no");

                    do //If the user enter invalid input it will display the choices again
                    {
                    levleUp = Console.ReadLine();
                    if (levleUp == "yes")
                    {
                    myHeroLevel++; //Add 1 everytime hero level up (Used for the equipment)
                        Console.WriteLine(" ");
                        Console.WriteLine("New level: " + Leveling.AddLevel(myHero)); //Go to page Leveling, and show new level
                        if(mageAt != 0) //If the mage were choosen as the hero
                        {
                            Leveling.AddMageAttribute(myHeroMage); //Go to Leveling page and add to the base attributes to the mage
                        } 
                        else if (rangerAt != 0) //If the ranger were choosen as the hero
                    {
                            Leveling.AddRangerAttribute(myHeroRanger); //Go to Leveling page and add to the base attributes to the ranger
                    } 
                        else if(rogueAt != 0) {
                            Leveling.AddRogueAttribute(myHeroRogue);
                        }
                        else
                        {
                            Leveling.AddWarriorAttribute(myHeroWarrior);
                        }
                    Console.WriteLine(" ");
                    Console.WriteLine("Do you wish to level up?\n yes / no" + myHeroName); //Display so yser can choose to up how many times they want
                    }   
                    else if (levleUp == "no")
                    {
                        Console.WriteLine("OK!");
                        addLevelUp = false; //Break the loop
                }
                    else
                    {
                        Console.WriteLine("Invalid input");
                    }

                } while (addLevelUp == true);
            bool addItem = true;
            do //If the user enter invalid input it will display the choices again
            {
                Console.WriteLine(" ");
                Console.WriteLine("Do you want to equip a weapon?\n yes / no"); //Ask if user want to equip a weapon
                string Equip = Console.ReadLine().ToLower();
                if(Equip == "yes")
                {
                    Equipment.EquipItems(EquipmentCheck, myHeroLevel); //Send choosen hero and the level to Equipment page (Weapons)
                    addItem = false;
                } 
                else if (Equip == "no")
                {
                    Console.WriteLine("OK!");
                    addItem = false;
                }
                else
                {
                    Console.WriteLine("Invalid input");
                }
            } while (addItem == true);

            bool addArmor = true;
            do
            {
                Console.WriteLine(" ");
                Console.WriteLine("Do you wish to equip armor?\n yes / no"); //Ask if user want to equip armor
                string armorAdd = Console.ReadLine().ToLower();
                if (armorAdd == "yes")
                {
                    Equipment.ArmorEquip(EquipmentCheck ,myHeroLevel); //Send choosen hero and the level to Equipment page (Armor)
                    addArmor = false;
                }
                else if (armorAdd == "no")
                {
                    Console.WriteLine("OK!");
                    addArmor = false;
                }
                else
                {
                    Console.WriteLine("Invalid input");
                }

            } while (addArmor == true);   
        }
    }
}
