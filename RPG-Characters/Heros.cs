﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public class Hero : Items //My base hero class, it inherits from Items page
    {
        public string heroName;
        public int level = 1;
        public int damage = 100;
    }
    public class Mage : Hero //My mage class, it inherits from Hero and has the base attributes
    {
        public int strength = 1;
        public int dexterity = 1;
        public int intelligence = 8;
    }
    public class Ranger : Hero //My ranger class, it inherits from Hero and has the base attributes
    {
        public int strength = 1;
        public int dexterity = 7;
        public int intelligence = 1;
    }
    public class Rogue : Hero
    {
        public int strength = 2;
        public int dexterity = 6;
        public int intelligence = 1;
    }
    public class Warrior : Hero
    {
        public int strength = 5;
        public int dexterity = 2;
        public int intelligence = 1;

        public string equipItem(Weapons testAxe)
        {
            throw new CustomExeptions();
        }

        public string equipItem(Armor testPlate)
        {
            throw new CustomExeptions();
        }
    }
}
