﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public class Armor : Items
    {
        public Armor(string name, int level, object sLOT_BODY, object aRMOR_PLATE)
        {
            Name = name;
            Level = level;
            SLOT_BODY = sLOT_BODY;
            ARMOR_PLATE = aRMOR_PLATE;
        }

        public Dictionary<ItemSlot, string> ArmorTypes { get; set; }
        public string Name { get; }
        public int Level { get; }
        public object SLOT_BODY { get; }
        public object ARMOR_PLATE { get; }

        public enum ArmorType
        {
            Cloth,
            Leather,
            Mail,
            Plate
        }
    }
}
