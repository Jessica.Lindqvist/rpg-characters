﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public class Items
    {
        public string Name { get; set; }
        public int LevelReq { get; set; }
        public string Slot { get; set; }

        public Dictionary<ItemSlot, string> Weapon { get; set; }
        public Dictionary<ItemSlot, string> Armor { get; set; }

        
        //public Dictionary<ItemSlot, Armor.ArmorType> Armor { get; set; }
        //public Dictionary<string,ItemSlot> Armor { get; set; }
        //public Items()
        //{
        //    Armor.Add(ItemSlot.HEAD_ARMOR_SLOT,"Cloth");
        //    Armor.Add(ItemSlot.CHEST_ARMOR_SLOT,"Cloth");
        //    Armor.Add(ItemSlot.LEG_ARMOR_SLOT,"Cloth");
        //    Weapon.Add(ItemSlot.WEAPON_SLOT, "Axe");
        //}
    } 
    
}
