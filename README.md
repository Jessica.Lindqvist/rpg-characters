# RPG-Characters

## Name
RPG Character creation

## Description
A console application where you can create a hero, level them up, and equip weapons and armor.

## Installation
Make sure you have Visual Studio, preferebly 2019 or later, installed on your computer.

## Usage
Start the application in Visual Studio and create your hero.

## Roadmap
Might make it so weapon and armor equipment are in a loop so if you try to add an item that are too high level or for another class then yours you can enter another item.

## Author
Jessica Lindqvist

## Project status
Done for now, might make minor fixes in the future (See Roadmap).
