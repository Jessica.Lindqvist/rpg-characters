using System;
using RPG_Characters;
using Xunit;
using static RPG_Characters.Armor;
using static RPG_Characters.Weapons;

namespace RPG_CharacterTesting
{
    public class UnitTest1
    {
        #region Character test
        [Fact]
        public void CharacterCreation_CharacterIsLvl1()
        {
            //Arrange
            Mage TestMage = new Mage();
            int expected = 1;

            //Act
            int actual = TestMage.level;

            //Assert
            Assert.Equal(expected, actual);

        }

        //[Fact]
        //public void CharacterCreation_CharacterLvlUp_CharacterIsLvl2() //I dont know how to test leveling up, I think I must rewrite how I set my heros up for that
        //{
        //    Mage TestMage = new Mage();
        //    int expected = 1;

        //    //Act
        //    int actual = TestMage.Leveling;

        //    //Assert
        //    Assert.Equal(expected, actual);
        //}
        #endregion

        #region Base character attribute
        [Fact]
        public void CharacterCreationMage_CharacterBaseAttributeIsCorrect()
        {
            //Arrange
            Mage TestMage = new Mage();
            int expectedDex = 1;
            int expectedInt = 8;
            int expectedStr = 1;

            //Act
            int actualDex = TestMage.dexterity;
            int actualInt = TestMage.intelligence;
            int actualStr = TestMage.strength;

            //Assert
            Assert.Equal(expectedDex, actualDex);
            Assert.Equal(expectedInt, actualInt);
            Assert.Equal(expectedStr, actualStr);
        }
        [Fact]
        public void CharacterCreationRanger_CharacterBaseAttributeIsCorrect()
        {
            //Arrange
            Ranger TestRanger = new Ranger();
            int expectedDex = 7;
            int expectedInt = 1;
            int expectedStr = 1;

            //Act
            int actualDex = TestRanger.dexterity;
            int actualInt = TestRanger.intelligence;
            int actualStr = TestRanger.strength;

            //Assert
            Assert.Equal(expectedDex, actualDex);
            Assert.Equal(expectedInt, actualInt);
            Assert.Equal(expectedStr, actualStr);
        }
        [Fact]
        public void CharacterCreationRogue_CharacterBaseAttributeIsCorrect()
        {
            //Arrange
            Rogue TestRogue = new Rogue();
            int expectedDex = 6;
            int expectedInt = 1;
            int expectedStr = 2;

            //Act
            int actualDex = TestRogue.dexterity;
            int actualInt = TestRogue.intelligence;
            int actualStr = TestRogue.strength;

            //Assert
            Assert.Equal(expectedDex, actualDex);
            Assert.Equal(expectedInt, actualInt);
            Assert.Equal(expectedStr, actualStr);
        }
        [Fact]
        public void CharacterCreationWarrior_CharacterBaseAttributeIsCorrect()
        {
            //Arrange
            Warrior TestWarrior = new Warrior();
            int expectedDex = 2;
            int expectedInt = 1;
            int expectedStr = 5;

            //Act
            int actualDex = TestWarrior.dexterity;
            int actualInt = TestWarrior.intelligence;
            int actualStr = TestWarrior.strength;

            //Assert
            Assert.Equal(expectedDex, actualDex);
            Assert.Equal(expectedInt, actualInt);
            Assert.Equal(expectedStr, actualStr);
        }

        //[Fact]
        //public void CharacterCreationMage_CharacterBaseAttributeIsCorrect_WhenLevelingUp() //I dont know how to test leveling up, I think I must rewrite how I set my heros up for that
        //{
        //    Mage TestMage = new Mage();

        //    TestMage.LevelinUp(){
        //    int expectedDex = 2;
        //    int expectedInt = 13;
        //    int expectedStr = 2;
        //    }


        //    //Act
        //    int actualDex = TestMage.dexterity;
        //    int actualInt = TestMage.intelligence;
        //    int actualStr = TestMage.strength;

        //    //Assert
        //    Assert.Equal(expectedDex, actualDex);
        //    Assert.Equal(expectedInt, actualInt);
        //    Assert.Equal(expectedStr, actualStr);
        //}
        #endregion

        #region Equipment test
        [Fact]
        public void CharacterEquipItem_EquipHighItemLevelWeapon_ThrowCustomException() //The tests are passing, I am unsure if they should, I dont really understand Unit testing
        {
            //Arrange
            Warrior testWarrior = new Warrior();
            Weapons testAxe = new Weapons("Common Axe", 2, ItemSlot.WEAPON_SLOT, WeaponType.Axe);
            //Act & Assert
            Assert.Throws<CustomExeptions>(() => testWarrior.equipItem(testAxe));
        }
        [Fact]
        public void CharacterEquipItem_EquipHighItemLevelArmor_ThrowCustomException() //The tests are passing, I am unsure if they should, I dont really understand Unit testing
        {
            //Arrange
            Warrior testWarrior = new Warrior();
            Armor testPlate = new Armor("Common plate body armor", 2, ItemSlot.CHEST_ARMOR_SLOT, ArmorType.Plate);
            //Act / Assert
            Assert.Throws<CustomExeptions>(() => testWarrior.equipItem(testPlate));
        }
        [Fact]
        public void CharacterEquipItem_EquipWrongWeaponType_ThrowCustomException()
        {
            //Arrange
            Warrior testWarrior = new Warrior();
            Weapons testBow = new Weapons("Common Bow", 1, ItemSlot.WEAPON_SLOT, WeaponType.Bow);
            //Act / Assert
            Assert.Throws<CustomExeptions>(() => testWarrior.equipItem(testBow));
        }
        [Fact]
        public void CharacterEquipItem_EquipWrongArmorType_ThrowCustomException()
        {
            //Arrange
            Warrior testWarrior = new Warrior();
            Armor testCloth = new Armor("Common cloth head armor", 1, ItemSlot.CHEST_ARMOR_SLOT, ArmorType.Cloth);
            //Act / Assert
            Assert.Throws<CustomExeptions>(() => testWarrior.equipItem(testCloth));
        }
        #endregion

        #region Equip valid item tests
        //[Fact] 
        //public void CharacterEquipItem_EquipAValidWeapon_ReturnASuccessMessage() //I dont know how to test valid item, I think I must rewrite how I set my heros up for that
        //{
        //    //Arrange
        //    Warrior testWarrior = new Warrior();
        //    Weapons testAxe = new Weapons("Common Axe", 1, ItemSlot.WEAPON_SLOT, WeaponType.Axe);
        //    string expected = "New weapon equipped!";
        //    //Act
        //    string actual = testWarrior.equipItem(testAxe); //It just throws my customExeption
        //    //Assert
        //    Assert.Equal(expected, actual);
        //}
        //[Fact]
        //public void CharacterEquipItem_EquipAValidArmor_ReturnASuccessMessage()
        //{
        //    //Arrange
        //    Warrior testWarrior = new Warrior();
        //    Armor testPlate = new Armor("Common plate body armor", 1, ItemSlot.CHEST_ARMOR_SLOT, ArmorType.Plate);
        //    string expected = "New armor equipped!";
        //    //Act
        //    string actual = testWarrior.equipItem(testPlate); //It just throws my customExeption
        //    //Assert
        //    Assert.Equal(expected, actual);
        //}
        #endregion

    }
}
